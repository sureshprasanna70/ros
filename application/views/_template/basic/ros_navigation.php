
<nav role="navigation">
    <ul class="nav nav-pills wooden-navigation">
        <li><a href="<?echo base_url()?>">Home</a></li>
        <li><a href="<?echo base_url()?>loadLevel">PLAY</a></li>
        <li><a href="<?echo base_url()?>leaderboard">Leaderboard</a></li>
        <li><a href="<?echo base_url()?>comments">Forum</a></li>
        <li><a href="<?echo base_url()?>instruction">FAQ</a></li>

        <ul class="nav pull-right nav-pills wooden-navigation">


         <?
         if($logged_in)
         {
            ?>
            <li class="pull-right wooden-navigation" name="k-connect-modal"
            <a data-toggle="modal" href="#login" class="k-login-button" style="display:none;">
                Login</a>
            </li>
            <li name="k-connect-profile" class="dropdowns">
               <a href="javascript:void(0)" name="attempt-logout">Logout</a>

           </li>
           <?
       }
       else
       {
        ?>
        <li name="k-connect-modal"><a data-toggle="modal" data-target="#login"> Login</a></li>
        <li name="k-connect-profile" class="dropdowns" style="display:none;">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">

            </a>
            <ul class="dropdown-menu">
                <li><a href="<? echo base_url(); ?>profile">Profile</a></li>
                <!-- <li><a href="#">Change Password</a></li> -->
                <li class="divider"></li>
                <li><a href="javascript:void(0)" name="attempt-logout">Logout</a></li>
            </ul>
        </li>
        <?
    }
    ?>
</li>
</ul>
</nav>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times; CLOSE</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">


                <div class="row">
                    <div class="col-lg-12">


                        <p class="login-title">Login with email</p>
                     
                            <div class="row">
                                <div class="col-lg-12">
                                    <div name="login-message"></div>
                                    <div name="register-message"></div>
                                </div>
                            </div>

                      
                        <form name="loginform" role="form" action="javascript:void(0)" method="POST" accept-charset="UTF-8">
                            <input type="email" name="emailaddress" placeholder="Email Address" required>
                            <input type="password" name="password" placeholder="Password" required>
                            <button type="submit"  name="attempt-login">Sign In <i class="fa fa-arrow-right"></i></button>
                        </form>
                        <br>(or)<br>
                        <a data-toggle="modal"  href="#registerModal" name="register-button">Register</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div id="registerform">
                    <div id="registerlogin">
                        <a href="javascript:void(0)" class="close"  name="close-button">Close <i class="fa fa-times-circle-o"></i></a>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="login-title">Register</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div name="register-message"></div>
                            </div>
                        </div>
                        <div class="row">
                            <form name="registerform" role="form" action="javascript:void(0)" method="POST" accept-charset="UTF-8">
                                <input type="text" name="name" value="<?echo set_value('name');?>" placeholder="Name" required>
                                <input type="email" name="email" value="<?echo set_value('email');?>" placeholder="Email Address" required>
                                <input type="password" name="password"  value="<?echo set_value('password');?>" placeholder="Password" required>
                                <input type="password" name="spassword"  value="<?echo set_value('spassword');?>" placeholder="Confirm Password" required>
                                <button type="submit"  name="attempt-register"><i class="fa fa-arrow-right"></i>Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>