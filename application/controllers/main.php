<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	private $cms_db;
	private $message;
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('bitauth');

		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('gravatar');
		$this->load->helper('kimage');
		$this->load->model('rosmodel');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		
		$this->db = $this->load->database('default',TRUE);
//		$this->cms_db = $this->load->database('cms',TRUE);
		
	}

	public function index()
	{
		
		//$data['title'] = "Kurukshetra 2014 | The Battle of Brains";
		$data['logged_in'] = 0;
		$data['sidebar'] = 2;
		$data['log']=0;

		$data['nav0'] = 1;
		$data['nav1'] = 0;
		$data['nav2'] = 0;
		$data['nav3'] = 0;
		$data['nav4'] = 0;
		$data['nav5'] = 0;
		$data['nav6'] = 0;
		$data['nav7'] = 0;

		$data['system_type'] = "home";

		
		$data['updates'] = 0;
		

		if($this->bitauth->logged_in())
		{
		
        	$ret=$this->session->all_userdata();
        	$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);
        	$data['log']=$logged_details;
        	
        	$data['logged_in'] = 1;
		}
			
			$data['message']=$this->message;
			$this->load->view('_template/head/doctype');
			$this->load->view('_template/head/html-start');
			$this->load->view('_template/basic/ros_navigation',$data);
			$this->load->view('_template/basic/roswelcome',$data);
			$this->load->view('_template/head/body-end');
			
	}

	public function ros($url=null)
	{

			if($this->bitauth->logged_in())
			{


			$ret=$this->session->all_userdata();
			$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);
			
				
				$this->loadLevel($level);

			

	}
	else
	{
		redirect('/');}
}

	public function submit()
	{
		
		if($this->bitauth->logged_in())
		{
			
			
			$data['logged_in']=1;
			$ret=$this->session->all_userdata();
			$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);
			$data['log'] =$logged_details;
			$answer=$this->rosmodel->getanswer($this->input->post('level'));
			
			if(strcmp(strtolower($this->input->post('answer')),$answer)==0)
				{
							
        			$this->rosmodel->promote($this->input->post('level'), $logged_details->kid);
					$this->ros('some');
           		}
			
			else
			{

			$me=rand(5, 15);
			$data['img']='meme'.$me.'.jpg';;
			$data['title']="ROS";
			$this->load->view('_template/head/doctype');
			$this->load->view('_template/head/html-start');
			$this->load->view('_template/basic/ros_navigation',$data);
			$this->load->view('_template/basic/roswelcome',$data);
			$this->load->view('_template/head/body-end');
			
			}
		}
		else
		{
			echo "hellO";
		}

}
		
	
	public function talaash_level($url=null)
	{
		
		if($this->bitauth->logged_in())
		{

		$ret=$this->session->all_userdata();
		$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);

		$data['log']=$logged_details;
			
				$user=$this->rosmodel->getLevel($logged_details->kid);
				$data['level']=$user['level'];
				$level=$user['level'];

				if($user['level']<=12 && $user['flag']!=1){
				switch($level)
				{
				case 1:$data['level']=1;
						$data['img']=array('alaasht.jpg');
						$data['pageclue']="The boot shaped country will help you find your way";
						break;
				case 2:$data['level']=2;
						$data['img']=array('alashta.jpg');
						$data['pageclue']="Tennis";
						break;
				case 3:$data['level']=3;
						$data['img']=array('aaslhta.png');
						$data['pageclue']="Die Antwort muss in Englisch erfolgen";
						break;
				case 4:$data['level']=4;
						$data['img']=array('athlsaa.jpg');
						$data['pageclue']="";
						break;
				case 5:$data['level']=5; 
						$data['img']=array('asaathl.jpg');
						$data['pageclue']="";
						break;
				case 6:$data['level']=6;
						$data['img']=array('thlaasa.jpg');
						$data['pageclue']="Death Date";
						break;
				case 7:$data['level']=7;
						$data['img']=array('XX.jpg');
						$data['pageclue']="Kill him";
						break;
				case 8:$data['level']=8;
						$data['img']=array('sathlaa.jpg');
						$data['pageclue']="Please don’t expect all the questions to have clues in the source code";
						break;
				case 9:$data['level']=9;
						$data['img']=array('thlasaa.jpg');
						$data['pageclue']="me,me,me,me";
						break;		
				case 10:$data['level']=10;	
						$data['img']=array('hlatsaa.jpg');
						$data['pageclue']="";
						break;
				case 11:
						$data['level']=11;
						$data['img']=array('artist.jpg');
						$data['pageclue']="NUVK EUA GXK GRR LKKROTM RAIQE";
						
						break;
				case 12:$data['level']=12;
						$data['img']=array('final.jpg');
						$data['pageclue']="";
						break;
				}
				
				$data['logged_in']=1;		
			
			
			$data['title']="Talaash'15";
			$this->load->view('_template/head/doctype');
			$this->load->view('_template/head/html-start');
			$this->load->view('_template/basic/ros_meta');
		
			$this->load->view('_template/basic/ros_navigation',$data);
			$this->load->view('questions',$data);
			$this->load->view('_template/head/body-end');
			}
			else{
				$this->message="You have been flagged";
				$this->index();
			}
			
			
			
		}
		else
		{
			redirect(base_url());
		}

	}
	public function loadLevel($level)
	{
		if($this->bitauth->logged_in())
		{
		$ret=$this->session->all_userdata();
		$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);

		
		$level=$this->rosmodel->getLevel($logged_details->kid,$logged_details->name);
		//$level=$this->rosmodel->getLevel($logged_details->kid);
		switch($level)
			{
				case 1:$data['level']=1;
						$data['urlclue']='q1';
						break;
				case 2:$data['level']=2;
						$data['urlclue']='q2';
						break;
				case 3:$data['urlclue']='q3';
						break;
				case 4:$data['urlclue']='q4';
						break;
				case 5:$data['urlclue']='q5';
						break;
				case 6:$data['urlclue']='q6';
						break;
				case 7:$data['urlclue']='XX';
						break;
				case 8:$data['urlclue']='q8';
						break;
				case 9:$data['urlclue']='q9';
						break;
				case 10:$data['urlclue']='q10';
						break;
				case 11:$data['urlclue']='q11';
						break;
				case 12:$data['urlclue']='q12';
						break;	
				
			}
			
			$url=base_url().'talaash_level/'.$data['urlclue'];
			redirect($url);
		}
		else
			redirect(base_url());

	}
	public function instruction()
	{
			$data['logged_in']=0;
			$data['title']="Talaash|INSTRUCTION";
			$this->load->view('_template/head/doctype');
			$this->load->view('_template/head/html-start');
			$this->load->view('_template/basic/ros_meta');
			$this->load->view('_template/basic/ros_navigation',$data);
			$this->load->view('_template/basic/ros_rules');
			$this->load->view('_template/head/body-end');
			
			
			
	}
	public function comments()
	{
			if($this->bitauth->logged_in())
			{
			$data['logged_in']=1;
			$ret=$this->session->all_userdata();
			$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);
			$data['log']=$logged_details;
			$data['title']="Talaash'15";
			$level=$this->rosmodel->getLevel($logged_details->kid);
			if($logged_details->kid == '481499')
			$level = $this->uri->segment(2);
			$data['level']=$level;	
			$this->load->view('_template/head/doctype');
			$this->load->view('_template/head/html-start');
			$this->load->view('_template/basic/ros_navigation',$data);
			$this->load->view('_template/basic/ros_comments.php',$level);
			$this->load->view('_template/head/body-end');
			}
			else
				{
					redirect(base_url());
				}
	}
	public function leaderboard()
	{
		
		
		$result=$this->db->query('SELECT kid,level,name from ros_main ORDER BY level DESC,timestamp ASC LIMIT 0,15');

		if($this->bitauth->logged_in())
		{
			$data['logged_in']=1;
			$ret=$this->session->all_userdata();
			$logged_details=$this->bitauth->get_user_by_id($ret['ba_user_id']);
			$data['log']=$logged_details;
			$data['count']= $this->db->count_all('ros_main');
			$rank=1;
			$user_id=$this->db->query('SELECT * from ros_main ORDER BY level DESC,timestamp ASC');
			//echo $count;
			foreach($user_id->result() as $detail)
				{
					//echo strcmp($logged_details->kid,$detail->kid);
					if(strcmp($logged_details->kid,$detail->kid)!=0)
						{
							$rank=$rank+1;
						}
				else
					break;
				}
			$data['rank']=$rank;
			$data['id']=$result->result();
			$this->load->view('_template/head/doctype');
			$this->load->view('_template/head/html-start');
			
			
			$this->load->view('_template/basic/ros_meta');
			$data['title']="Talaash'15";
			$this->load->view('_template/basic/ros_navigation',$data);
			$this->load->view('_template/basic/leader',$data);
			$this->load->view('_template/head/body-end');
		}
		else
			redirect(base_url());
	}

}	
